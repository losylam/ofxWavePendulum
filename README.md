ofxWavePendulum
====

Description
----

Simulation d'un "Wave Pendulum" dans le cadre de la résidence de Laurent Malys à l'Université du Havre.

Environnement
---

Application [Openframeworks](www.openframeworks.cc) qui permet de contrôler un synthétiseur réalisé avec [Supercollider](http://supercollider.github.io/).

Le fichier supercollider du synthéthiseur est situé dans bin/data/wave_pendulum.scd

![Capture d'écran](ofxWavePendulum.png "Capture d'écran")
