#include "ofApp.h"

int last_n;

//--------------------------------------------------------------
void ofApp::setup(){
  height = ofGetScreenHeight();
  width = ofGetScreenWidth();
  
  parameters.setName("cycloid");
  
  parameters.add(n_pendulum.set("nb pendules", 10, 0, 50));
  parameters.add(freq_base.set("freq base", 1, 0, 10));
  parameters.add(freq_mul.set("freq mul", 1.5, 0, 5));
  
  parameters.add(vit_deph.set("vitesse", 0.1, 0, 1));

  parameters.add(mode_center.set("center circles", false));
  parameters.add(mode_line.set("lines (or circles)", false));
  parameters.add(mode_h_line.set("h lines", false));

  gui.setup(parameters);
  
  cpt = 0;

  last_n = n_pendulum;

  sender.setup("localhost", 6666);
}

//--------------------------------------------------------------
void ofApp::update(){
  if (last_n != n_pendulum){

    ofxOscMessage m;
    m.setAddress("/pendulum/n");
    m.addIntArg(n_pendulum);
    sender.sendMessage(m, false);
    last_n = n_pendulum;
  }
}

//--------------------------------------------------------------
void ofApp::draw(){
  ofBackgroundGradient(ofColor(100), ofColor(10));
  ofNoFill();
  h_pendulum = height/n_pendulum;

  ofLine(width/2, 0, width/2, height);
  for (int p = 0; p < n_pendulum; p++){
    ofPushMatrix();
    ofTranslate(0, h_pendulum*(p+0.5));
    ofBeginShape();
    for (int i = 0; i < width; i+= 1){
      ofVertex(i, h_pendulum * cos((freq_base * (1 + (p * freq_mul)) * (i+cpt)) * PI/180)/2);
    }
    ofEndShape();
    ofFill();
    float pos_mil = cos((freq_base * (1 + (p * freq_mul)) * (width/2+cpt)) * PI/180)/2;
    ofColor(200);

    if (mode_center){
      ofDrawCircle(width/2, h_pendulum  * pos_mil, 20);
    }
    ofColor(0);
    if (mode_line){
      ofRect(ofMap(pos_mil, -0.5, 0.5, 0, width), -h_pendulum/2, 10, h_pendulum);
    }else{
      ofSetColor(255, 127);
      ofDrawCircle(ofMap(pos_mil, -0.5, 0.5, 0, width), 0, h_pendulum/2);
      ofSetColor(255);
      ofDrawCircle(ofMap(pos_mil, -0.5, 0.5, 0, width), 0, h_pendulum/4);
    }

    if(mode_h_line){
      ofLine(0, pos_mil*h_pendulum, width, pos_mil*h_pendulum);
    }
    ofNoFill();
    ofPopMatrix();
    ofxOscMessage m;
    m.setAddress("/pendulum/amp");
    m.addIntArg(p);
    m.addFloatArg(pos_mil);
    sender.sendMessage(m, false);
  }
  

  cpt+=vit_deph;
  //freq_mul = ofMap(mouseY, 0, height, 0.0f, 2.0f);

  gui.draw();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
  switch(key){
  case 'f':
    ofToggleFullscreen();
  }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
